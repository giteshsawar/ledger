import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { makeSelectProduct } from './selectors';

import { toJS } from 'immutable';

import Product from '../../../components/Product/Product';
import * as ProductActions from './actions';

const mapStateToProps = createStructuredSelector({
  // console.log('state of Product', state.activeTabButton, state.Product, state.Product.toJS());
  productState: makeSelectProduct(),
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ProductActions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Product);
