// @flow
import { GET_SEARCH_INPUT, DECREMENT_COUNTER, SET_ACTIVE_TAB, SET_SEARCH_RESULT } from './actions';
import type { Action } from '../../reducers/types';
import { fromJS } from 'immutable';

export const initialState = fromJS({
  topSearch: '',
  activeTab: 'products',
  searchResult: []
});

export default function bottomBar(state = initialState, action: Action) {   
    switch (action.type) {
        default:
        return state;
    }
}
