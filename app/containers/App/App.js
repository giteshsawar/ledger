// @flow
import * as React from 'react';
import { toJS } from 'immutable';
import { compose, bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import * as AppActions from './actions';
import makeSelectApp, { makeSelectTopSearch } from './selectors';

import CssBaseline from '@material-ui/core/CssBaseline';
import Header from '../../components/Header/Header';
import BottomBar from '../BottomBar';
import { withRouter } from 'react-router-dom';

type Props = {
  children: React.Node,
};

class App extends React.Component<Props> {
  props: Props;

  render() {
    const { children, history, appState } = this.props;
    const { topSearch } = appState;
    console.log('app history props', history);
    const showBar = history.location.pathname !== '/';
    return (
      <React.Fragment>
        <CssBaseline />
        <div style={{ height: 'calc(100% - 70px)' }}>
          {showBar && <Header topSearch={topSearch} />}
          {children}
        </div>
        {showBar && <BottomBar />}
      </React.Fragment>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  appState: makeSelectApp(),
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators(AppActions, dispatch);
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

export default withRouter(compose(withConnect)(App));
