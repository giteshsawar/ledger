import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Input } from '@material-ui/core';

import Style from './style.scss';

type Props = {};

export default class SearchFilter extends Component<Props> {
  props: Props;

  render() {
    const { searchContainer, search, filters } = Style;
    return (
      <div className={searchContainer}>
        <div className={search}>
          <Input type="search" />
        </div>
        <div className={filters}>
          
        </div>
      </div>
    );
  }
}

SearchFilter.propTypes = {};
