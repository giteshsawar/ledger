import React, { Component } from 'react';
import PropTypes from 'prop-types';
// import SwipeableViews from 'react-swipeable-views';

import Styles from './style.scss';

type Props = {
  value: string,
};

function TabPanel(props) {
  const { children, value, index } = props;
  console.log('tab view props', children, index);
  return <p>{value}</p>;
}

TabPanel.propTypes = {
  children: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  index: PropTypes.string.isRequired,
};

export default class Views extends Component<Props> {
  props: Props;

  handleChangeIndex = () => {};

  render() {
    const { value } = this.props;
    const { activeView } = Styles;
    console.log('value of tab', value);
    // const theme = useTheme();
    // console.log('theme api', theme);
    return (
      // <SwipeableViews
      //   // dir={theme.direction}
      //   // axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
      //   index={value}
      //   onChangeIndex={this.handleChangeIndex}
      // >
      <div className={activeView}>
        <TabPanel value={value} index={0} />
      </div>
      // </SwipeableViews>
    );
  }
}
Views.propTypes = {};
