import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the party state domain
 */
const selectPartyDomain = state => state.party;

/**
 * Other specific selectors
 */
const makeSelectParty = () =>
createSelector(selectPartyDomain, substate => substate.toJS());

const makeSelectTopSearch = () => 
createSelector(selectPartyDomain, substate => substate.get('topSearch'));

const makeSelectActiveTab = () => 
createSelector(selectPartyDomain, substate => substate.get('activeTab'));

const makeSelectSearchResult = () => 
createSelector(selectPartyDomain, substate => substate.get('searchResult'));

export default makeSelectParty;
export {
    makeSelectParty,
    makeSelectTopSearch,
    makeSelectActiveTab,
    makeSelectSearchResult
};