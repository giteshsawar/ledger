// @flow
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { productList, partyList } from '../../data/data';

import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';

import List from './List';
import BottomBar from '../BottomBar/BottomBar';

import styles from './Dashboard.scss';
import homeStyles from '../Home/Home.scss';
import routes from '../../constants/routes';

type Props = {
  getSearchInput: () => void,
  incrementIfOdd: () => void,
  incrementAsync: () => void,
  decrement: () => void,
  setActiveTab: () => void,
  dashboardState: object,
};

export default class Dashboard extends Component<Props> {
  props: Props;

  state = {
    list: [],
  };

  setDashboardList = () => {
    console.log('productLisasdast', productList);
    const { activeTab } = this.props.dashboardState;

    let list = partyList;
    if (activeTab === 'products') {
      list = productList;
    }
    this.setState({ list });
  };

  componentDidUpdate(prevProps) {
    if (
      prevProps.dashboardState.activeTab !== this.props.dashboardState.activeTab
    ) {
      this.setDashboardList();
    }
  }

  componentDidMount() {
    this.setDashboardList();
  }

  render() {
    const {
      dashboardContainer,
      dashboardHeader,
      mainLogo,
      search,
      categoryTabs,
      resultsContainer,
      filteredResults,
      listItem,
      tabPrimary,
      tabSecondary,
      tabBtn,
      tabActive,
    } = styles;
    const { list } = this.state;
    const { dashboardState, setActiveTab } = this.props;
    const { activeTab } = dashboardState;
    console.log('activeTab', this.props.dashboardState);
    return (
      <div className={dashboardContainer}>
        <div className={categoryTabs}>
          <ButtonGroup fullWidth aria-label="full width outlined button group">
            <Button
              className={`${tabBtn} ${tabPrimary} ${
                activeTab === 'products' ? `${tabActive}` : ''
              }`}
              color="primary"
              onClick={() => setActiveTab('products')}
              size="large"
            >
              Products
            </Button>
            <Button
              className={`${tabBtn} ${tabSecondary} ${
                activeTab === 'parties' ? `${tabActive}` : ''
              }`}
              onClick={() => setActiveTab('parties')}
              size="large"
            >
              Parties
            </Button>
          </ButtonGroup>
        </div>
        <div className={resultsContainer}>
          <div className={filteredResults}>
            <List list={list} activeTab={activeTab} />
          </div>
        </div>
        {/* <BottomBar /> */}
      </div>
    );
  }
}
