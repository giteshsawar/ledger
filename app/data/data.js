export const productList = [
  {
    name: 'Curtains',
    cover:
      'https://capp-data.s3.ap-south-1.amazonaws.com/products/curtains.jpg',
    party: { name: 'Swayam Sigma' },
    balance: 20000,
  },
  {
    name: 'Mats',
    cover:
      'https://capp-data.s3.ap-south-1.amazonaws.com/products/download.jpeg',
    party: { name: 'Sampada synthetics' },
    balance: -15000,
  },
  {
    name: 'Fittings',
    cover:
      'https://capp-data.s3.ap-south-1.amazonaws.com/products/fittings.jpg',
    party: { name: 'TechnoCrafts' },
    balance: 10000,
  },
  {
    name: 'Bottles',
    cover: 'https://capp-data.s3.ap-south-1.amazonaws.com/products/bottles.jpg',
    party: { name: 'Ikraft' },
    balance: 33000,
  },
];

export const partyList = [
  {
    name: 'Swayam Sigma',
    cover:
      'https://capp-data.s3.ap-south-1.amazonaws.com/products/curtains.jpg',
  },
  {
    name: 'Sampada synthetics',
    cover:
      'https://capp-data.s3.ap-south-1.amazonaws.com/products/download.jpeg',
  },
  {
    name: 'TechnoCrafts',
    cover:
      'https://capp-data.s3.ap-south-1.amazonaws.com/products/fittings.jpg',
  },
  {
    name: 'Ikraft',
    cover: 'https://capp-data.s3.ap-south-1.amazonaws.com/products/bottles.jpg',
  },
];

const billsList = [
  {
    date: '2019-08-24T09:27:47.405Z',
    serial: 'bill012',
    amount: 10000,
    mode: 'cheque',
    attachments: []
  }
];
const paymentsList = [];
