import React from 'react';
import { Link } from 'react-router-dom';
import Styles from './style.scss';

function List(props) {
    const { listItem, image, name, rest, text } = Styles;
    console.log('listItem', listItem, props.activeTab);
    const link = props.activeTab === 'products' ? 'product' : 'party';
    return (
        <div className="dash-list">
            {props.list.map(item => (
            <Link to={`/${link}/${item.name}`}>
                <div className={listItem} key={item.name}>
                    <div className={image} style={{ background: `url('${item.cover}')` }}>
                    </div>
                    <div className={name}>
                        <div className={text}>{item.name}</div>
                    </div>
                    <div className={rest}>
                    </div>
                </div>
            </Link>
            ))}
        </div>
    );
}

export default List;