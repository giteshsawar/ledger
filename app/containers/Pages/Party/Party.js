import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { makeSelectParty } from './selectors';

import { toJS } from 'immutable';

import Party from '../../../components/Party/Party';
import * as PartyActions from './actions';

const mapStateToProps = createStructuredSelector({
  partyState: makeSelectParty(),
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators(PartyActions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Party);
