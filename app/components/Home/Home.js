// @flow
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import routes from '../../constants/routes';
import styles from './Home.scss';

type Props = {};

export default class Home extends Component<Props> {
  props: Props;

  render() {
    const { container, contentContainer, btn, btnMd, btnPrimary, btnSecondary } = styles;
    return (
      <div className={container} data-tid="container">
        {/* <Link to={routes.COUNTER}>to Counter</Link> */}

        <div className={contentContainer}>
          <h2>Authenticate yourself</h2>
          <Link to={routes.DASHBOARD}>
            <button type="button" className={`${btn} ${btnMd} ${btnPrimary}`} style={{ marginRight: '20px' }}>Login</button>
            <button type="button" className={`${btn} ${btnMd} ${btnSecondary}`}>Signup</button>
          </Link>
        </div>
      </div>
    );
  }
}
