// @flow
import { GET_SEARCH_INPUT, SET_ACTIVE_TAB, SET_SEARCH_RESULT } from './actions';
import type { Action } from '../../reducers/types';
import { fromJS } from 'immutable';

export const initialState = fromJS({
  topSearch: '',
  activeTab: 'products',
  searchResult: []
});

export default function party(state = initialState, action: Action) {
  switch (action.type) {
    case GET_SEARCH_INPUT:
      return state.set('topSearch', action.payload);
    case SET_ACTIVE_TAB:
      return state.set('activeTab', action.payload);
    case SET_SEARCH_RESULT:
      return state.set('searchResult', action.payload);
    default:
      return state;
  }
}
