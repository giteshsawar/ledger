import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the product state domain
 */
const selectProductDomain = state => state.product;

/**
 * Other specific selectors
 */
const makeSelectProduct = () =>
createSelector(selectProductDomain, substate => substate.toJS());

const makeSelectTopSearch = () => 
createSelector(selectProductDomain, substate => substate.get('topSearch'));

const makeSelectActiveTab = () => 
createSelector(selectProductDomain, substate => substate.get('activeTab'));

const makeSelectSearchResult = () => 
createSelector(selectProductDomain, substate => substate.get('searchResult'));

export default makeSelectProduct;
export {
    makeSelectProduct,
    makeSelectTopSearch,
    makeSelectActiveTab,
    makeSelectSearchResult
};