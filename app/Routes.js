import React from 'react';
import { Switch, Route } from 'react-router-dom';
import routes from './constants/routes';
import App from './containers/App/App';
import HomePage from './containers/Pages/Home/HomePage';
import Dashboard from './containers/Pages/Dashboard/Dashboard';
import PartyPage from './containers/Pages/Party/Party';
import ProductPage from './containers/Pages/Product/Product';
console.log('routes of app', routes);
export default () => (
  <App>
    <Switch>
      <Route exact path={routes.DASHBOARD} component={Dashboard} />
      <Route exact path={routes.PARTY} component={PartyPage} />
      <Route exact path={routes.PRODUCT} component={ProductPage} />
      <Route exact path={routes.HOME} component={HomePage} />
    </Switch>
  </App>
);
