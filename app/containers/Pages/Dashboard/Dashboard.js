import { compose, bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { makeSelectDashboard } from './selectors';

import { toJS } from 'immutable';

import Dashboard from '../../../components/Dashboard/Dashboard';
import * as DashboardActions from './actions';

const mapStateToProps = createStructuredSelector({
  // console.log('state of dashboard', state.activeTabButton, state.dashboard, state.dashboard.toJS());
  dashboardState: makeSelectDashboard(),
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators(DashboardActions, dispatch);
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

export default compose(withConnect)(Dashboard);
