import React, { Component } from 'react';
import Styles from './Header.scss';

type Props = {};

export default class Header extends Component<Props> {
  props: Props;

  getInput = e => {
    const { value } = e.target;
    this.props.getSearchInput(value);
  };

  render() {
    const { headerContainer, mainLogo, search } = Styles;
    const { topSearch } = this.props;
    return (
      <div className={headerContainer}>
        <div className={mainLogo}></div>
        <div className={search}>
          <input
            type="text"
            value={topSearch}
            onChange={e => this.getInput(e)}
            placeholder={`Search...`}
          />
        </div>
      </div>
    );
  }
}
