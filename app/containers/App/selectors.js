import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the app state domain
 */
const selectAppDomain = state => state.app;

/**
 * Other specific selectors
 */
const makeSelectApp = () =>
  createSelector(
    selectAppDomain,
    substate => substate.toJS()
  );

const makeSelectTopSearch = () =>
  createSelector(
    selectAppDomain,
    substate => substate.get('topSearch')
  );

export default makeSelectApp;
export { makeSelectTopSearch };
