// @flow
import type { GetState, Dispatch } from '../reducers/types';

export const GET_SEARCH_INPUT = 'GET_SEARCH_INPUT';
export const SET_ACTIVE_TAB = 'SET_ACTIVE_TAB';
export const SET_SEARCH_RESULT = 'SET_SEARCH_RESULT';

export function getSearchInput(data) {
  return {
    type: GET_SEARCH_INPUT,
    payload: data,
  };
}

export function setActiveTab(data) {
  return {
    type: SET_ACTIVE_TAB,
    payload: data,
  }
}

export function setSearchResult(data) {
  return {
    type: SET_SEARCH_RESULT,
    payload: data,
  }
}

// export function incrementIfOdd() {
//   return (dispatch: Dispatch, getState: GetState) => {
//     const { counter } = getState();

//     if (counter % 2 === 0) {
//       return;
//     }

//     dispatch(increment());
//   };
// }

// export function incrementAsync(delay: number = 1000) {
//   return (dispatch: Dispatch) => {
//     setTimeout(() => {
//       dispatch(increment());
//     }, delay);
//   };
// }