// @flow
import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import { fromJS } from 'immutable';
import { LOCATION_CHANGE } from 'react-router-redux';

import app from '../containers/App/reducer';
import dashboard from '../containers/Pages/Dashboard/reducer';
import bottomBar from '../containers/BottomBar/reducer';
import product from '../containers/Pages/Product/reducer';
import party from '../containers/Pages/Party/reducer';

// Initial routing state
const routeInitialState = fromJS({
  location: null,
});
const appInitialState = fromJS({
  data: 'HELLO WORLD',
});
/**
 * Merge route into the global application state
 */
export function routeReducer(state = routeInitialState, action) {
  switch (action.type) {
    /* istanbul ignore next */
    case LOCATION_CHANGE:
      return state.merge({
        location: action.payload,
      });
    default:
      return state;
  }
}

export default function createRootReducer(history: History) {
  return combineReducers({
    router: connectRouter(history),
    route: routeReducer,
    app,
    dashboard,
    bottomBar,
    product,
    party,
  });
}
