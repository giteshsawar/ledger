// @flow
import type { GetState, Dispatch } from '../reducers/types';

export const GET_SEARCH_INPUT = 'GET_SEARCH_INPUT';
export const DECREMENT_COUNTER = 'DECREMENT_COUNTER';
export const SET_ACTIVE_TAB = 'SET_ACTIVE_TAB';
export const SET_SEARCH_RESULT = 'SET_SEARCH_RESULT';

export function getSearchInput(data) {
  return {
    type: GET_SEARCH_INPUT,
    payload: data,
  };
}

export function decrement() {
  return {
    type: DECREMENT_COUNTER
  };
}

export function setActiveTab(data) {
  return {
    type: SET_ACTIVE_TAB,
    payload: data,
  }
}

export function setSearchResult(data) {
  return {
    type: SET_SEARCH_RESULT,
    payload: data,
  }
}
