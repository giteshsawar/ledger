import React, { Component } from 'react';
import PropTypes from 'prop-types';

import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

type Props = {
  value: string,
  handleChange: () => void,
};

export default class AppTabs extends Component<Props> {
  props: Props;

  render() {
    const { value } = this.props;
    console.log('valuevalue', value);
    return (
      <div>
        <AppBar position="static" color="default">
          <Tabs
            value={value}
            onChange={this.props.handleChange}
            indicatorColor="primary"
            textColor="primary"
            variant="fullWidth"
            aria-label="full width tabs example"
          >
            <Tab label="Bills" value="bills" />
            <Tab label="Payments" value="payments" />
            <Tab label="Information" value="information" />
          </Tabs>
        </AppBar>
      </div>
    );
  }
}

AppTabs.propTypes = {
  views: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  handleChange: PropTypes.func.isRequired,
};
