import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the dashboard state domain
 */
const selectDashboardDomain = state => state.dashboard;

/**
 * Other specific selectors
 */
const makeSelectDashboard = () =>
  createSelector(
    selectDashboardDomain,
    substate => substate.toJS()
  );

const makeSelectActiveTab = () =>
  createSelector(
    selectDashboardDomain,
    substate => substate.get('activeTab')
  );

const makeSelectSearchResult = () =>
  createSelector(
    selectDashboardDomain,
    substate => substate.get('searchResult')
  );

export default makeSelectDashboard;
export { makeSelectDashboard, makeSelectActiveTab, makeSelectSearchResult };
