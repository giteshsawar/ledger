// @flow
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { productList } from '../../data/data';

import OpenInNewOutlinedIcon from '@material-ui/icons/OpenInNewOutlined';
import AccountBalanceWalletOutlinedIcon from '@material-ui/icons/AccountBalanceWalletOutlined';

import AppTabs from '../Common/Tabs/Tabs';
import Views from '../Common/View/View';
import SearchFilter from '../Common/SearchFilter/SearchFilter';

import styles from './Product.scss';
import homeStyles from '../Home/Home.scss';

type Props = {
  setActiveTab: () => void,
  productState: object,
};

export default class Product extends Component<Props> {
  props: Props;

  state = {
    value: 'bills',
  };

  handleChange = (e, value) => {
    this.setState({ value });
  };

  render() {
    const {
      productPage,
      productHeader,
      cover,
      intro,
      name,
      partyName,
      balance,
      negative,
      overlay,
      controls,
    } = styles;
    const { darkGreenOverlay } = homeStyles;
    const { productName } = this.props.match.params;
    const { value } = this.state;
    const product = productList.find(p => p.name === productName);
    console.log('product page props', this.props, product);
    return (
      <div className={productPage}>
        <div className={productHeader}>
          <div
            className={cover}
            style={{ background: `url('${product.cover}')` }}
          >
            <div className={`${darkGreenOverlay} ${overlay}`}>
              <OpenInNewOutlinedIcon />
            </div>
          </div>
          <div className={intro}>
            <p className={name}>{product.name}</p>
            <p className={partyName}>{product.party.name}</p>
            <p className={`${balance} ${product.balance < 0 && negative}`}>
              <span>
                <AccountBalanceWalletOutlinedIcon />
              </span>{' '}
              {product.balance > 0 ? '+' : ''}
              {product.balance}
            </p>
          </div>
        </div>
        <div className={controls}>
          <div className="tabs">
            <AppTabs handleChange={this.handleChange} value={value} />
          </div>
          <SearchFilter />
        </div>
        <div className="product-content">
          <Views value={value} />
        </div>
      </div>
    );
  }
}
