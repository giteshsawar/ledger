// @flow
import { SET_ACTIVE_TAB, SET_SEARCH_RESULT } from './actions';
import type { Action } from '../../reducers/types';
import { fromJS } from 'immutable';

export const initialState = fromJS({
  activeTab: 'products',
  searchResult: [],
});

export default function dashboard(state = initialState, action: Action) {
  switch (action.type) {
    case SET_ACTIVE_TAB:
      return state.set('activeTab', action.payload);
    case SET_SEARCH_RESULT:
      return state.set('searchResult', action.payload);
    default:
      return state;
  }
}
