// @flow
import type { GetState, Dispatch } from '../reducers/types';

export const GET_SEARCH_INPUT = 'GET_SEARCH_INPUT';

export function getSearchInput(data) {
  return {
    type: GET_SEARCH_INPUT,
    payload: data,
  };
}
