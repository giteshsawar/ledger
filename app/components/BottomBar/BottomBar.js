// @flow
import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import HomeOutlinedIcon from '@material-ui/icons/HomeOutlined';
import TrendingUpOutlinedIcon from '@material-ui/icons/TrendingUpOutlined';
import NotificationsNoneIcon from '@material-ui/icons/NotificationsNone';
import PersonOutlinedIcon from '@material-ui/icons/PersonOutlined';
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';

import Style from './BottomBar.scss';

type Props = {
    bottomBarState: object,
    dashboardState: object
};
export default class BottomBar extends Component<Props> {
    props: Props;

    render() {
        const { bottomBar, menuContainer, menuItem, centerButton, iconBox, primary, secondary } = Style;
        const { bottomBarState, dashboardState } = this.props;
        const { activeTab } = dashboardState;
        console.log('button classes', activeTab);
        return (
            <div className={bottomBar}>
              <div className={menuContainer}>
                <div className={menuItem}>
                    <Link to="/dashboard">
                        <HomeOutlinedIcon />
                    </Link>
                </div>
                <div className={menuItem}>
                    <TrendingUpOutlinedIcon />
                </div>
                <div className={centerButton}>
                    <Fab className={`${activeTab === 'products' ? `${primary}` : `${secondary}`}`} aria-label="add">
                        <AddIcon />
                    </Fab>
                </div>
                <div className={menuItem}>
                    <NotificationsNoneIcon />
                </div>
                <div className={menuItem}>
                    <PersonOutlinedIcon />
                </div>
              </div>
            </div>
        );
    }
}