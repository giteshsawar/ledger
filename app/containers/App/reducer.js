// @flow
import { GET_SEARCH_INPUT } from './actions';
import type { Action } from '../../reducers/types';
import { fromJS } from 'immutable';

export const initialState = fromJS({
  topSearch: '',
});

export default function app(state = initialState, action: Action) {
  switch (action.type) {
    case GET_SEARCH_INPUT:
      return state.set('topSearch', action.payload);
    default:
      return state;
  }
}
