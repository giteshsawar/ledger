// @flow
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { partyList } from '../../data/data';

import styles from './Party.scss';

type Props = {
  setActiveTab: () => void,
  partyState: object
};

export default class Party extends Component<Props> {
  props: Props;

  render() {
    console.log('party page props', this.props);
    return (
      <div>Party page</div>
    );
  }
}