import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { toJS } from 'immutable';

import BottomBar from '../../components/BottomBar/BottomBar';
import * as BottomBarActions from './actions';

function mapStateToProps(state) {
    console.log('state', state);
  return {
    bottomBarState: state.bottomBar.toJS(),
    dashboardState: state.dashboard.toJS(),
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(BottomBarActions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BottomBar);
